const express = require('express')
const list = require('./list.json')

const app = express()
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs')


app.get('/', (req, res)=>{
    res.render('index', {list: list})
})


const PORT = 3000
app.listen(PORT, ()=> console.log(`Server running on port ${PORT}`))